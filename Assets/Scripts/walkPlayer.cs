﻿using UnityEngine;
using System.Collections;

public class walkPlayer : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
    public float speed = 10.0F;
    public float rotationSpeed = 100.0F;
    
    public GameObject projectile;
    public float fireRate = 0.5F;
    private float nextFire = 0.0F;

	// Update is called once per frame
	void Update () {
        float translation = Input.GetAxis("Vertical") * speed;
        float rotation = Input.GetAxis("Horizontal") * rotationSpeed;
        translation *= Time.deltaTime;
        rotation *= Time.deltaTime;
        transform.Translate(0, 0, translation);
        transform.Rotate(0, rotation, 0);
        //なんかうまく動かん(´・ω・`)
        if (Input.GetButton("Fire1") && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            GameObject clone = Instantiate(projectile, transform.position, transform.rotation) as GameObject as GameObject;
        }
	}
}
